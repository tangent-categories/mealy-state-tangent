{-# OPTIONS_GHC -Wno-unrecognised-pragmas #-}
{-# HLINT ignore "Use tuple-section" #-}
module Generator where

import Control.Monad.Free hiding (unfold)
import Control.Monad.Codensity

import Control.Monad.State.Strict
import Control.Monad.Identity

import Data.Bifunctor
import Data.Distributive (Distributive(distribute))

type MealyStep a b = (a,b) -> (a,b)


-- https://stackoverflow.com/questions/31358856/continuation-monad-for-a-yield-await-function-in-haskell
-- https://github.com/purescript-contrib/purescript-machines/blob/v5.1.0/src/Data/Machine/Mealy.purs#L50-L50
-- https://stackoverflow.com/questions/27997155/finite-state-transducers-in-haskell
-- https://hackage.haskell.org/package/generator-0.5.5/docs/Control-Monad-Generator.html
-- https://hackage.haskell.org/package/concurrency-1.11.0.2/docs/Control-Monad-Conc-Class.html

-- newtype MealyT f s a = MealyT (s -> f (Step f s a))
-- Mealy m s a b = Yield b | Step (s -> )

-- | We are now going to formally combine the state (mealy) monad transformer with the resumption monad (free monad)
newtype MealyStateT m a b c = Step (a ->  m (b, c))

type MealyState a b c = MealyStateT Identity a b c
type FakeState s a = MealyState s a s

-- | Note that state embeds into MealyState
-- type FakeState s a = MealyState s a s -- FakeState s a = MealyState s a s = Step (s -> (a,s)) = State s a 

instance Functor m => Bifunctor (MealyStateT m a) where
    -- f :: b -> b'
    -- g :: c -> c'
    -- h :: a -> m (b,c)
    -- @desired_result :: a -> m (b',c')
    -- bimap f g :: (b,c) -> (b',c')
    -- fmap (bimap f g) :: m (b,c) -> m (b',c')
    -- fmap (bimap f g) . h :: a -> m (b',c')
    bimap f g (Step h) = Step $ fmap (bimap f g) . h

instance Functor m => Functor (MealyStateT m a b) where
    fmap = second


type MealyT m a b = Free (MealyStateT m a b)
type FastMealyT m a b = Codensity (MealyStateT m a b)

data DirectMealyT m a b c = End c | Continue (a -> m (b,DirectMealyT m a b c))

mealyTToDirectMealyT :: Functor m => MealyT m a b c -> DirectMealyT m a b c
mealyTToDirectMealyT s = case s of
  Pure c -> End c
  Free (Step f) -> Continue (fmap (second mealyTToDirectMealyT) . f)

directMealyTToMealyT :: Functor m => DirectMealyT m a b c -> MealyT m a b c
directMealyTToMealyT s = case s of
  End c -> Pure c
  Continue f -> Free (Step (fmap (second directMealyTToMealyT) . f))




{-
Recall 
Free m a = End a | Step (m (Free m a))
By observations in Delay.hs, we have that 

type DelayT m a = Free m a = ResumptionT m a

Hence 

MealyT m a = Free (MealyStateT m a b) = DelayT (MealyStateT m a b) = ResumptionT (MealyStateT m a b)
So then, to get classical Mealy machines, we use 
Mealy a b x
  = MealyT Identity a b c
  = ResumptionT (MealyStateT Identity a b) c
  \equiv ResumptionT c. (a -> Identity (b,c))
  \equiv ResumptionT c. (a -> (b,c))
  \equiv R a b c 
where 
  R a b c = End c | Step (a -> (b,R a b c))

Then recall
StatefulResumption s a = StatefulResumptionT s Identity a 
                       = ResumptionT (StateT s Identity) a 
                       = Resumption (State s) a 
                       \equiv S s a 
where 
    S s a = End a | Step (s -> (s,S s a))

Hence StatefulResumptions embed into Mealy because 

StatefulResumption s a \equiv S s a 
    = (End a | Step (s -> (s,S s a))) 
    \equiv (End a | Step (s -> (s,R s s a)))
    \equiv Mealy R s s a
-}

{-
Since Mealy machines are also resumptions, just like for StatefulResumptions we have a step and faststep
-}

stepPure :: Functor f => (a -> f (b,c)) -> MealyT f a b c
stepPure = step . Step

step :: (Functor f) => MealyStateT f a b c -> MealyT f a b c
step = liftF

-- stepFast 
-- stepPureFast

-- The following gets really close to coroutines, the exception being that after a yield (Pure), execution immediately stops, whereas 
-- for a true coroutine, a yield can occur while the computation continues.  Generators only really create reactive programs.
-- However, ..., generators are bisimilar to coroutines, because, of trampolining.  
-- Await is a way to call a coroutine that promises to return something, and runs it until it returns.
-- In this implementation, awaiting should never block...  Need to test.
await :: Functor f => MealyT f a b c -> a -> Either c (f b)
await (Free (Step mac)) = Right . fmap fst . mac
await (Pure c)  = const (Left c)

evolve :: Functor f => MealyT f a b c -> a  -> Either c (f ( b,MealyT f a b c))
evolve (Pure c) = const $ Left c
evolve (Free (Step mac)) = Right . mac

-- We can also write the yield/emit function
yield :: Applicative f => b -> MealyT f a b ()
yield b = step $ Step $ \ _a -> pure (b,())

-- And now finally, we can write Python code in Haskell
doStuff :: (Applicative f, Num t) => t -> MealyT f a t b
doStuff i = do
    yield i
    doStuff (i+1)

-- Machines can also be unfolded from a specification of a single step
unfoldGeneratorT :: Functor m => (s -> Either c (a -> m (s,b))) -> s -> MealyT m a b c
unfoldGeneratorT spec init = case spec init of
  Left c -> Pure c
  -- f :: a -> m (s,b)
  Right f ->
      -- idea: define nextStep :: a -> m (b,MealyT m a b c) so that we can Step nextStep
      -- result :: m (s,b)
      -- then we feed the s in result to unfoldGeneratorT
      -- but we have to fmap the recursion in since result :: m (s,b)
    let nextStep a = let result = f a in fmap (\(nextState,b) -> (b,unfoldGeneratorT spec nextState)) result
    in Free (Step nextStep)

unfoldMealyT :: Functor m => ((s,a) -> m (s,b)) -> s -> MealyT m a b c
-- transSpec :: ((s,a) -> m (s,b))
-- curry transSpec :: s -> a -> m (s,b)
-- Suppose g :: x -> y
-- Then Right . g :: x -> Either z y
-- Right . curry transSpec :: s -> Either z (a -> m (s, b))
unfoldMealyT transSpec  = unfoldGeneratorT spec
    where spec = Right . curry transSpec

unfoldGeneratorPure :: (s -> Either c (a -> (s,b))) -> s -> MealyT Identity a b c
unfoldGeneratorPure spec = unfoldGeneratorT liftSpec
    where liftSpec = second (\f a -> return (f a) ) . spec

unfoldMealyPure :: ((s,a) -> (s,b)) -> s -> MealyT Identity a b c
unfoldMealyPure spec = unfoldMealyT liftSpec
    where liftSpec  =  return . spec

-- Todo change all instances of MealyT to GeneratorT




-- We also have streams
data Stream a = S
    {
        headS :: a,
        tailS :: Stream a
    }

instance Functor Stream where
  fmap f s = S {headS = f (headS s) , tailS = fmap f (tailS s)}


unfoldStream :: (c -> a) -> (c -> c) -> c -> Stream a
unfoldStream h t v = S {headS = h v,tailS = unfoldStream h t (t v)}

ones :: Stream Int
ones = unfoldStream id id 1

zeros :: Stream Int
zeros = unfoldStream id id 0

nats :: Stream Integer
nats = unfoldStream id (+1) 0

type Stack a = Stream a

push :: a -> Stack a -> Stack a
push x s = S {headS = x,tailS=s}

pop :: Stack a -> (a,Stack a)
pop s = (headS s,tailS s)

-- From stacks, we can build Queues
-- However, one may notice, that empty Queues aren't really possible with the stack interface.
-- What we kind of need are lists

-- We also have an interface to lists, using MealyT
-- Again, this is just for proof of concept.  In practice, one should use faster data types.
-- For example, an opaque type for some of this stuff with special language support for the needed operations 
type List a = MealyT Identity () a ()
  -- = End () | Continue (() -> (a,Queue a))
  -- = Nil | Cons a (Queue a)
emptyList :: List a
emptyList = Pure ()

cons :: a -> List a -> List a
cons a l = case l of
  Pure _ -> Free $ Step $ \() -> return (a,Pure ())
  Free mst -> do
      stepPure (\() -> Identity (a,())) -- create a
      l -- then run the rest of the list

reverseL :: List a -> List a
reverseL l = reverseLHelper l emptyList

reverseLHelper :: List a -> List a -> List a
reverseLHelper l acc = case l of
  Pure x0 -> acc
  Free (Step m) -> let (a,t) = runIdentity (m ()) in reverseLHelper t (cons a acc)


toListM :: List a -> [a]
toListM l = case l of
  Pure _ -> []
  Free (Step m) -> let (a,t) = runIdentity (m ()) in a : toListM t



-- | Amortized constant times queues from Lists.
data Queue a = Queue (List a ) (List a)

newQueue :: Queue a
newQueue = Queue emptyList emptyList

isEmptyQueue :: Queue a -> Bool
isEmptyQueue (Queue (Pure _) (Pure _)) = True
isEmptyQueue (Queue _ _) = False

enq :: Queue a -> a -> Queue a
enq (Queue xs ys) z = Queue xs (cons z ys)

deq :: Queue a -> Maybe (a,Queue a)
deq (Queue (Pure _) (Pure _)) = Nothing -- an empty queue can't be dequeued
deq (Queue (Pure _) ys) = deq (Queue (reverseL ys) emptyList) -- if the front of the queue is depleted, reverse the back of the queue and make it the front
deq (Queue (Free (Step m)) ys) = let (x,xs) = runIdentity (m ()) in Just (x,Queue xs ys)

-- Here we show how to set up an Erlang/Actor style concurrency

-- data DirectMealyT m a b c = End c | Continue (a -> m (b,DirectMealyT m a b c))




-- For those worried about the use of ints... we could sub in nats, but I mean ...
type Delay a = Free Identity a
type Nat = Delay ()

data NatDirect = Zero | Succ NatDirect

natToNatDirect :: Nat -> NatDirect
natToNatDirect n = case n of
  Pure () -> Zero
  Free (Identity fr) -> Succ (natToNatDirect fr)

natDirectToNat :: NatDirect -> Nat
natDirectToNat n = case n of
  Zero -> Pure ()
  Succ nd -> Free (Identity (natDirectToNat nd))




-- I suppose we could make an environment in different ways.  All of them are compatible with differential programming.
-- Essentially, the moment we have free monads...
-- The following is a terribly inefficient search tree.  One could keep it balanced and do fancy stuff but we won't here.
data TreeShape a = Branch a a

instance Functor TreeShape where
    fmap f (Branch x y) = Branch (f x) (f y)

type Tree = Free TreeShape
type Env a b = Tree (a,b)

-- And then we can use Trees to give the abstract interface for non-empty environments
-- Foregoing efficiency for proof of concept
singletonEnv :: a -> b -> Env a b
singletonEnv a b = Pure (a,b)

lookupEnv :: Eq a => a -> Env a b -> Maybe b
lookupEnv x (Pure (y,z)) = if x == y then Just z else Nothing
lookupEnv x (Free (Branch t1 t2)) =
    case lookupEnv x t1 of
        Nothing -> lookupEnv x t2
        v -> v

{-
If a key is attempted to be inserted in an environment where the key already exists, then the environment is returned unchanged.
-}
insertEnv :: Eq a => a -> b -> Env a b -> Env a b
insertEnv key val env = case lookupEnv key env of
    Nothing -> Free (Branch env (singletonEnv key val))
    _ -> env

{-
If a key exists in the environment then adjustEnv will replace the value with the new value.
Again, purposefully foregoing effeciency.
-}
adjustEnv :: Eq a => a -> b -> Env a b -> Env a b
adjustEnv key val (Pure (y,val_old)) = if key == y then singletonEnv key val else Pure (key,val_old)
adjustEnv key val (Free (Branch t1 t2)) = do
    t1' <- adjustEnv key val t1
    t2' <- adjustEnv key val t2
    liftF $ Branch t1' t2'

{-
Insert a key value into the environment, overwriting in case the key already exists
-}
insertEnvDestructive :: Eq a => a -> b -> Env a b -> Env a b
insertEnvDestructive key val env = case lookupEnv key env of
    Nothing -> Free (Branch env (singletonEnv key val))
    _ -> adjustEnv key val env


-- Now we start to move a bit more
type PID = Int
type ProcessAbstract m a b c = (PID,MealyT m a b c,Queue a,Queue b)

-- However, in reality, we probably want c to represent concrete signals, and for a and b to be either the same, or maybe slightly better to implement to and from JSon or something like that
-- That is what we'll do next
-- runGeneratorOnList :: Generator a b c -> [a] -> [b]
-- runGeneratorOnList (Pure _c) _xs = []
-- runGeneratorOnList _m [] = []
-- runGeneratorOnList (Free (Step m)) (x:xs) = 
--     let (y,m') = m x 
--     in let ys = runGeneratorOnList m' xs 
--     in (y:ys)

type PureGeneratorT m b c = MealyT m () b c

processStream :: (Monad m) => MealyT m a b c -> Stream a -> PureGeneratorT m b c
processStream mac str = case mac of
  Pure c -> return c
  Free (Step m) -> do
    let x =  m (headS str)
    Free $ Step $ \ _ -> fmap (\(b,tr) -> (b,processStream tr (tailS str))) x




-- Pure generators are potentially haltable streams aka potentiall infinite nonempty lists
-- A pure generator can be transformed into a stream (of eithers) or a colist
-- If a pure generator generates outputs and signals of the same type, then it may be transformed into a stream
pureGenToStreamEither :: (Monad m) => PureGeneratorT m a b -> m (Stream (Either a b))
pureGenToStreamEither gen = case popGen gen of
  Left b -> do
    res <- pureGenToStreamEither gen
    return $ S {headS = Right b,tailS = res}
  Right m -> do
    (next,tailG) <- m
    res <- pureGenToStreamEither tailG
    return $ S {headS = Left next,tailS = res}



pureGenToEitherStream :: (Monad m) => PureGeneratorT m a b -> m (Either (Stream a) b)
pureGenToEitherStream gen = case popGen gen of
  Left b -> return $ Right b
  Right m -> do
    (next,tailG) <- m
    res <- pureGenToEitherStream tailG
    case res of
      Left st -> return $ Left st
      Right b -> return $ Right b


pureGenToColist :: (Monad m) => PureGeneratorT m a b -> m [a]
pureGenToColist gen = case popGen gen of
  Left b -> return []
  Right m -> do
    (next,tailG) <- m
    res <- pureGenToColist tailG
    return $ next:res

{-
A pure generator that yields signals of the same type as its output is bisimilar to a colist.
Either a a -> a
-}
anyWay :: Either a a -> a
anyWay e = case e of
  Left a -> a
  Right a -> a

pureGenToPureStream :: (Monad m) => PureGeneratorT m a a -> m (Stream a)
pureGenToPureStream gen = fmap anyWay <$> pureGenToStreamEither gen


pureGenToPureColist :: (Monad m) => PureGeneratorT m a a -> m [a]
pureGenToPureColist gen = case popGen gen of 
  Left a -> return [a]
  Right m -> do 
    (next,tailG) <- m 
    res <- pureGenToPureColist tailG 
    return $ next:res


-- Of course, it's better to view pure generators by their own interface
-- They can support a stack like interface that sends a signal at the end of the stack
pushGen :: (Applicative m) => a -> PureGeneratorT m a b -> PureGeneratorT m a b
pushGen x mac = do
  yield x
  mac

popGen :: PureGeneratorT m a b -> Either b (m (a,PureGeneratorT m a b))
popGen mac = case mac of
  Pure b -> Left b
  Free (Step m) -> Right $ m ()

{-
Here is the "true" essence of generators -- they process pure generators
-}
processPureGen :: (Monad m) => PureGeneratorT m a b -> (b -> d) -> MealyT m a c d -> m (PureGeneratorT m c d)
processPureGen gen f mac =
  case popGen gen of
    Left b -> return $ return (f b)
    Right magen -> do
      (a,gen2) <- magen  -- head and tail of generator
      case evolve mac a of
        Left d -> return $ return d
        Right mc -> do
          (val,resum) <- mc
          resumProc <- processPureGen gen2 f resum
          return $ pushGen val resumProc




{-
Interpretation: 
If the machine is just at the end, then it's Pure b
But also, if pushing the button once returns Pure, then it's also at the end
-}


data PureGeneratorTDirect m a b = Nil b | Cons (m (a,PureGeneratorTDirect m a b))



yieldA :: Applicative f => f b -> MealyT f a b ()
yieldA b = step $ Step $ \ _a ->  fmap (\x -> (x,())) b

