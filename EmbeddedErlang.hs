{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-} 
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE BangPatterns #-}
module EmbeddedErlang where

{-
Embedding task parallelism concurrency in Haskell using Free monads, and a few ideas floating around.
-}
import Control.Monad.Free hiding (unfold)
import Control.Monad.Codensity

import Data.Sequence (Seq)
import qualified Data.Sequence as S
import Data.Map (Map)
import qualified Data.Map as M

import Control.Monad.State.Strict
import Control.Monad.Identity
import Control.Monad.Except
import Data.Bifunctor
import Text.JSON
import Data.Aeson.Types
import Control.Applicative ((<|>))
import Prelude hiding ((/))

import Control.Monad.Free.Class


{-
Concurrency is really just a trick with Free monads.  Aka Delay monads.  Thus it's really a trick with free w-CPOs.  The trick 
is that Free w-CPOs can be delayed -- that's kind of their point.  So a computation is a thing that says that we do one bit of computation now,
and then delay the rest for later.

Indeed: the Delay/Free w-CPO transformer is 
    data Delay m a = Now a | Later m (Delay m a)

Of course, note that the free monad on m is 
    data Free m a = Pure a | Free (m (Free m a))

Thus the classical Delay monad of Tarmo is equivalently 
   type Delay a = Free Identity a = Now a | Later (Delay a)

To see non-efficient implementations for some of the auxiliary functions and datastructures used here, 
in an immediately differentiable way, see Generator.hs.

We also import Codensity to give an asymptotic improvement of Free monads, by reassociating all the binds.
So we can just write all our code and at the end write `improve program` and be done with it... 
-}

{-
A (pure) reactor has an immediate value of some type, and a function that can be used to respond a message.
The ReactorT type allows for effectful responses to messages.
-}
data ReactorT tycon next inp out = R next (inp -> tycon out)

instance (Functor tycon) => Functor (ReactorT tycon next inp) where
    fmap f (R n g) = R n (fmap f . g)

type ReactT m next inp out = Free (ReactorT m next inp) out

{-
Since ReactorT is a functor in the "out" variable, it follows that ReactT is a monad.
We can also give a direct representation of ReactT.

It is maybe a bit easier to describe the intuition for Reactive computations from this point of view.
A reactive process can either halt with a termination signal of type termSignal.  Or it can be paused 
for later completion (rescheduling).  This allows the computation to proceed "one step at a time".  
The way we have encoded this however, the response can take an arbitrary amount of time -- that is 
-- we do not force "atomic" actions as done in the Harrison and Procter paper.  Though atomicity is certainly
required for realtimeness, and a sort of guarantee of reactive progress, we are only attempting to 
capture the overall idea of scheduling processes here.
-}
data ReactTDirect effect request response termSignal = Done termSignal | Paused request (response -> effect (ReactTDirect effect request response termSignal))

{-
Here are the functions involved in the isomorphism.
-}
reactToRD :: (Functor m) => ReactT m next inp out -> ReactTDirect m next inp out
reactToRD r = case r of
  Pure out -> Done out
  Free (R next f) -> Paused next (fmap reactToRD . f)

rdToReact :: (Functor m) => ReactTDirect m next inp out -> ReactT m next inp out
rdToReact r = case r of
  Done out -> Pure out
  Paused next f -> Free (R next (fmap rdToReact . f))


{-
Note we can lift a reactor to a reactive computation.
-}
liftReactor :: (Functor f) => ReactorT f a b c -> ReactT f a b c
liftReactor = liftF

liftReactorPure :: (Functor f) => a -> (b -> f c)  -> ReactT f a b c
liftReactorPure next = liftReactor . R next


{-
We can also yield reactive computations
-}
yield :: (Applicative f) => next -> out -> ReactT f next inp out
yield n o = liftReactor $ R n (\ _i -> pure o)

yieldA :: (Applicative f) => next -> f out -> ReactT f next inp out
yieldA n o = liftReactor $ R n (const o)

data GuardError = GuardError

yieldGuarded :: (Monad f) => next -> (inp -> Bool) -> out -> ReactT (ExceptT GuardError f) next inp out
yieldGuarded n p o = liftReactor $ R n (\ i -> if p i then return o else throwError GuardError)

yieldAGuarded :: (Monad f) => next -> (inp -> Bool) -> f out -> ReactT (ExceptT GuardError f) next inp out
yieldAGuarded n p o = liftReactor $ R n (\ i -> if p i then lift o else throwError GuardError)

{-
That's it.  The above provides the abstract interface required to interleave computations.  However, to really 
showcase how it works, we'll adapt https://harrisonwl.github.io/assets/papers/hosc-cheapthreads.pdf 
and make a "green threads" kernel of sorts.  However, we won't be as complete as their implementation.
-}
class Msg a where


instance Msg Int where

instance Msg String where

instance Msg JSValue where  -- for JSON ala Text.JSON 

instance Msg Value where    -- for JSON ala Aeson

{-
Queues for messages
-}
type Queue a = Seq a

emptyQ :: Queue a
emptyQ = S.empty

enq :: Queue a -> a -> Queue a
enq q a = q S.|> a

deq :: Queue a -> Maybe (a,Queue a)
deq q = case S.viewl q of
  S.EmptyL -> Nothing
  (h S.:< t) -> Just (h,t)

{-
We now give the machine that expresses one essence of reactive concurrency.
Note that we are not claiming any effeciency here -- we are just providing a means 
of embedding interleaving in our language.  Note that with an IO monad transformer 
thrown into the stack, we could get something that looks a lot like concurrency.

The basic idea for this machine comes Turner's total functional programming.
"An operating system can be considered as a function from a stream of re-
quests to a stream of responses. To program things like this functionally we need
infinite lists - or something equivalent to infinite lists"
 -- https://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.106.364&rep=rep1&type=pdf

Rather than operate on a stream of requests and responses, we operate on queues.
Reactive computations then generate requests and await responses.  The function itself 
is created by the interaction of the reactive computational steps.

Finally, we take a motivation from Erlang.  In Erlang, processes send messages directly 
to another process' PID.  We can model this  interaction (without regard to efficiency) in an event-driven fashion with the above ideas as follows:
We model reactive erlangy processes as inhabitants of ReactT m a b c.
A process declares the intent to a send a message to another process and then to continue later upon receiving a message with
    R (Send message PID) reactionHook
The kernel then sees this and places the process back in the resumption map with an altered request:
    R Waiting reactionHook
And then adds the message to the message queue.
    For PID message PID
to indicate that the first PID should receive the message and that it was sent by the second PID.
Then the kernel alerts the process with that PID, and proceeds to evolve according to the process' reaction.
In case the process is finished.  It responds back with its termination message.
In case the process the process is waiting, the reactionHook is handed the message.
In case the process is paused with Send message PID, then we're back to the beginning.

Of course, we also need to say what happens when the messages run out or the processes have all finished, which we do below.
The main idea is that when a process ends, it ends with a default message which we think of as a signal sent to anyone attempting to 
communicate with the halted process.  Thus, the process forever ignores the content of any messages sent to it and immediately responds 
with its halt signal.  This means that the process is a static message responder.  We thus create a "service" a much simpler entity than a process
to simply return the message whenever a message is sent to the PID that is now gone.
-}
{- 
We'll take a standard thread id approach -- a PID is just a number.
-}
type PID = Integer
data Request msg = Waiting |  Send msg PID                            -- Modelled as 1 + msg x N
data MsgProc msg = For PID msg PID                                    -- Modelled as N x msg x N
type MsgQueue msg = Queue (MsgProc msg)                               -- Modelled as Queue (MsgProc msg) where Queue a is modelled as two lists (see Generator.hs)
type Process eff msg = ReactT eff (Request msg) msg msg         -- Modelled as ReactT eff msg (Request msg) msg as defined above
data Status = MsgStat | ProcStat                                      -- Modelled as 1 + 1
type ResMap eff msg = Map PID (Process eff msg)                        -- Modelled as N \to (ReactT ...) + 1  where N is 1+1...  -- if the PID is in the map it is Just the process.  Alternatively N is forall b. b -> (b -> b) -> b == \int_b [b,[[b,b],b]] where \int_b denotes the categorical end.
type StaticService msg = Map PID msg                                    -- Modelled as N \to msg + 1 (see ResMap)



-- The kernel state is given by a resumption map, a queue of messages, the status, and the process currently in scope.
-- Note that in this simple model, we do not attempt to alert if a process writes to a non-existent PID.  The PID may simply not 
-- yet exist, in which case the message sits there waiting for the PID to come alive and consume it.  
data Kernel eff msg = Mac (ResMap eff msg) (StaticService msg) (MsgQueue msg) Status PID -- Modelled as (ResMap ...) x StaticService x (MsgQueue ...) x Status x N

{-
A step of the machine moves the machine by one evolution, accumulates any effects, and indicates if it's done.
-}
kernelStep ::  (Applicative eff) => Kernel eff msg -> eff (Either (Kernel eff msg) ())
kernelStep m@(Mac _ _ _ MsgStat _) = messageStep m
kernelStep m@(Mac _ _ _ ProcStat _) = processStep m


{-
Handle a single message on the message queue.
-}
messageStep :: (Applicative eff) => Kernel eff msg -> eff (Either (Kernel eff msg) ())
messageStep (Mac procs svcs msgs _ curr) = case S.viewl msgs of
  -- When there are no messages left, we then force the current process to evolve
  S.EmptyL -> pure $ Left $ Mac procs svcs msgs ProcStat curr
  -- Otherwise we process the current message by looking up the PID of the who the message is for
  (For toPid msg fromPid) S.:< seq -> case M.lookup toPid procs of
    -- if the PID doesn't exist, see if the PID was converted to a service
    Nothing -> case M.lookup toPid svcs of
      -- In this case the PID doesn't exist yet.  Since we don't have fork, we know it will never exist.  However,
      -- we use the semantics for if we did have fork, and simply move the message to the end of the queue, in case one 
      -- day we implement fork.  We then go to the processStep since the message did nothing.
      Nothing -> pure $ Left $ Mac procs svcs seq ProcStat curr
      -- In case the process has spawned a service, we simply relay the message back to the sender, and move to a process step.
      Just msg' -> pure $ Left $ Mac procs svcs (msgs S.|> For fromPid msg' toPid ) ProcStat curr
    -- In this case we have found the process with the PID to evolve by the message.
    -- We evolve according to the process.
    Just fr -> case fr of
      -- The case that the process is done tells us that the process completely ignores the message, and spawns a service 
      -- which forever returns that message.  We then move to the process step.  We also need to remove the process from the procs map.
      Pure msg' -> pure $ Left $ Mac (M.delete toPid procs) (M.insert toPid msg' svcs) msgs ProcStat curr
      -- The case that we have a response to make tells us to evolve by 1 step, and then continue on.
      -- If the process that was being messaged is waiting for a message, then an actual evolution occurs.
      Free (R Waiting resp) ->
        let
          evolvedResp = resp msg
          evolvedProcs = flip (M.insert toPid) procs <$> evolvedResp
          evolvedMac = (\ newProcs -> Mac newProcs svcs seq ProcStat curr) <$> evolvedProcs
        in
          Left <$> evolvedMac
      -- In case the process is found, but is not in the waiting state,
      -- we put its requested message into the message queue, which results in the process going into the waiting state,
      -- so we could simply return in the MessageStat with the modified message queue.
      -- However, for efficiency, we actually then immediately process the message.
      Free (R (Send newMsg newToPid) resp) ->
        let
          evolvedResp = resp msg
          evolvedProcs = flip (M.insert toPid) procs <$> evolvedResp
          newMsgs = seq S.|> For newToPid newMsg toPid -- this is the message queue minus the message that was responded to, with the new message from the process
          evolvedMac = (\ newProcs -> Mac newProcs svcs newMsgs ProcStat curr) <$> evolvedProcs
        in
          Left <$> evolvedMac

{-
Set up the current process for one move.
-}
processStep :: (Applicative eff) => Kernel eff msg -> eff (Either (Kernel eff msg) ())
processStep (Mac procs svcs msgs _ curr) = case M.lookup curr procs of
  -- If current process does not exist, we search for a new process.
  Nothing -> case M.minViewWithKey procs of
  -- If there are no processes, then there is nothing to evolve, and we are done.
    Nothing -> pure $ Right ()
    -- Otherwise we have found our new process, so we return in the proc stat with this new pid.
    Just ((newCurr,_),_) -> pure $ Left $ Mac procs svcs msgs ProcStat newCurr
  -- Otherwise our current process does exist, and we can set up the evolution for it.
  Just fr -> case fr of
    -- if the current process is done, then we set up a service in its name
    (Pure msg) -> pure $ Left $ Mac (M.delete curr procs) (M.insert curr msg svcs) msgs MsgStat curr
    -- otherwise we create a new message and move to the message step.
    (Free (R re f)) -> case re of
    -- If the process is waiting for a message, we immediately move to a message step
      Waiting -> pure $ Left $ Mac procs svcs msgs MsgStat curr
      -- otherwise, we enter the message to the message queue, set status to waiting, and then move to the message step
      (Send msg toPid) -> pure $ Left $ Mac (M.insert curr (Free (R Waiting f)) procs) svcs (msgs S.|> For toPid msg curr) MsgStat curr




{-
The running of the kernel runs the kernel until it's done.  As typical this requires choosing a "PID 1"
For efficiency, we made the kernel tail recursive on the above "small step".
-}
runKernel :: (Monad eff) => Kernel eff msg -> eff ()
runKernel k = do
  !res <- kernelStep k -- using a bang pattern we force the effect to accumulate.  This is probably a problem.  Should stick to a pure monadic interface and instead use "strict"/by-value monads.
  case res of
    Left ker -> runKernel ker
    Right _ -> pure ()

runProcesses :: (Monad eff) => [(PID,Process eff msg)] -> PID -> eff ()
runProcesses procs  = runKernel . Mac procMap M.empty S.empty ProcStat
  where procMap = foldr (uncurry M.insert) M.empty procs

{-
To improvements to the above model would be: 
  1) Forking
  2) Better garbage collection
  3) Pub-sub model

For 1, we could just add a new request type Fork (Process eff msg), and thread that through the Kernel.  Request eff msg then is modelled by 1 + msg x N + Process eff msg.
This would also introduce a few other considerations.  First, it would have to be decided what should happen when a process is attempted to be forked with a PID that 
already exists.  There are many possibilities here.  We could simply have both processes and do a sort of 1-to-many communication.  We could alternatively terminate the 
existing process.  We could also just refuse the spawn.  Also, it would have to be decided what should happen when a process is spawned and there is a service for that PID.
Again there are different possibilities.  We could remove the service for instance.

For 2, we could possibly improve what happens when a process ends.  Currently, when a process is done, it creates a service which persists forever.  Anyone that attempts 
to write to the old process' id is sent the message it set to be sent.  This creates a memory leak over time, so something better should be done.

For 3, we need to change how messages are handled.  In a pub-sub system a single message could trigger responses from multiple processes.  Thus in a pub-sub system, 
we would want a queue of PIDs and a queue of messages.  We would also change Waiting to indicate who the process wants to receive a message from.  Then 
for each message broadcast request, the message would be added added to the Queue but now we need only indicate who the message is From.  We then run through all the processes
in the process queue to see if they want to consume that message and either add them immediately to an aux queue or evolve them one step, adding any messages to the end of the message
queue, and then adding them to the aux queue.  When the process queue is exhausted, the aux queue becomes the process queue.  Of course, many improvements could be made to this model.
-}

{--
Of course, we should have a little fun with the kernel.  By recreating the classic ping-pong erlang tutorial.
We have two concurrent processes.  Ping sends a message "1" to pong.  And then waits for a response.
Pong receives the message "1" from ping and sends the message "2" to pong and then waits.
Ping receives the message "2" from pong and sends the message "1" to pong and then waits.
Ad infinitum.
-}
ping :: Process IO String
ping = Free (R (Send "1" 2) pingResp)

pingResp :: String -> IO ( Process IO String)
pingResp msg = do
  putStrLn msg
  return ping


pong :: Process IO String
pong = Free (R (Send "2" 1) pongResp)

pongResp :: String -> IO (Process IO String)
pongResp msg = do
  putStrLn msg
  return pong

{- |
Run this concurrent program on the command line.
-}
test :: IO ()
test = runProcesses [(1,ping),(2,pong)] 1

{- 
We can redo the pong example using a more free monadic interface.
This also emphasizes the tail recursion.
-}
ping2 :: Process IO String
ping2 = do
  liftReactorPure (Send "1" 2) putStrLn
  ping2

pong2 :: Process IO String
pong2 = do
  liftReactorPure (Send "2" 1) putStrLn
  pong2

test2 :: IO ()
test2 = runProcesses [(1,ping2),(2,pong2)] 1

{-
One of the convenient aspects of embedding the kernel in Haskell is that Haskell can be used as a 
well-typed macro language.

Here we create a Kleisli arrow:  for any msg and pid, send msg pid :: (a -> f b) -> ReactT ... i.e. a Kleisli arrow.
-}
send :: Functor f => msg -> PID -> (b -> f c) -> ReactT f (Request msg) b c
send msg pid =  liftReactorPure (Send msg pid)

wait :: Functor f => (b -> f c) -> ReactT f (Request msg) b c
wait = liftReactorPure Waiting

{-
Send gives an idiom for writing processes.
process = do
  let callback x = callback code 
  send msg pid callback 
  continueOn

We can use this idiom to rewrite our favourite example.
-}
ping3 :: Process IO String
ping3 = do
  let callback x = putStrLn $ "Ping received: " ++ x
  send "ping" 2 callback
  ping3

pong3 :: Process IO String
pong3 = do
  let callback x = putStrLn $ "Pong received: " ++ x
  send "pong" 1 callback
  pong3

test3 :: IO ()
test3 = runProcesses [(1,ping3),(2,pong3)] 1

{-
Of course we can also create a more canonical looking way to set up and run the processes.
-}
processes :: [(Integer, Process IO String)]
processes =
  start /
  spawn ping3 1 /
  spawn pong3 2






-- The end of the line/sequence operator like in C.  Using semicolon in Haskell without TH is not allowed, so we tried to find a symbol that was close enough.
-- Or we could use TH.  A program is just a list of processes to be started concurrently.
(/) :: (MonadPlus m) => m a -> m a -> m a
(/) = (<|>)

-- Tell a program to begin
start :: [a]
start = []

-- Spawn process b with pid a
spawn :: b -> a -> [(a, b)]
spawn a b = [(b,a)]

test4 :: IO ()
test4 = runProcesses processes 1





{-
We can redo the above example using a combinator that performs a step and then continues on doing the same thing forever.
Thus we are treating Haskell as a combinator/macro langauge again.
-}

repeatReact :: (Monad m) => (t -> m a) -> t -> m b
repeatReact kleisArr callback = do
  kleisArr callback
  repeatReact kleisArr callback

ping5 :: Process IO String
ping5 = repeatReact (send "1" 2) putStrLn

pong5 :: Process IO String
pong5 = repeatReact (send "2" 1) putStrLn



test5 :: IO ()
test5 = runProcesses prog 1
  where
    prog =
      start /
      spawn ping5 1 /
      spawn pong5 2


{-
Finally we comment on how the codensity monad can be used with some class to obtain asymptotic speedups.
Consider an alternate version of ping, but written classy.
-}
ping6 :: (MonadFree (ReactorT IO (Request String) String) m) => m String
ping6 = do
  liftF $ R (Send "1" 2) putStrLn
  ping6

-- Now ping6 can be auto-improved.
ping7 :: Process IO String
ping7 = improve ping6

{-
We can also rewrite our combinators send and wait to be classy.
-}

sendClassy :: (MonadFree (ReactorT tycon (Request msg) inp) m, Functor tycon) => msg -> PID -> (inp -> tycon a) -> m a
sendClassy msg pid resp = liftF $ R (Send msg pid) resp

waitClassy :: (MonadFree (ReactorT tycon (Request msg) inp) m, Functor tycon) => (inp -> tycon a) -> m a
waitClassy resp = liftF $ R Waiting resp

ping8 :: (MonadFree (ReactorT IO (Request String) String) m) => m String
ping8 = do
  let callback = putStrLn
  sendClassy "1" 2 callback
  ping8

ping9 :: (MonadFree (ReactorT IO (Request String) String) m) => m String
ping9 = repeatReact (sendClassy "ping" 2) putStrLn

pong8 :: (MonadFree (ReactorT IO (Request String) String) m) => m String
pong8 = do
  let callback = putStrLn
  sendClassy "2" 1 callback
  pong8

pong9 :: (MonadFree (ReactorT IO (Request String) String) m) => m String
pong9 = repeatReact (sendClassy "pong" 1) putStrLn

ping8Fast:: Process IO String
ping8Fast = improve ping8

ping9Fast :: Process IO String
ping9Fast = improve ping9

pong8Fast :: Process IO String
pong8Fast = improve pong8

pong9Fast :: Process IO String
pong9Fast = improve pong9

test8 :: IO ()
test8 = runProcesses program 1
  where
    program =
      start /
      spawn ping8Fast 1 /
      spawn pong8Fast 2

test9 :: IO ()
test9 = runProcesses program 1
  where
    program =
      start /
      spawn ping9Fast 1 /
      spawn pong9Fast 2

{-
Instead of threading the codensity bit through everything, we left it out to emphasize the concurrent embedding.
If some knows how to make this a bit nicer with something like the following:

runFast procs = runProcesses (map (second improve) procs) 1

I would love to know.  I know that one has to be careful when dealing with universally quantified things, but I thought 
this should be possible.  Ask on SO.

Oh, one thing to keep in mind for semantics on actually distributing stuff on multicore.  If the machine can be 
broken down into machines that communicate with each other by passing messages to each other ... then we should 
be able to get the semantics to be given by a machine in each CCC together with some distributed mechanism 
for keeping the list of messages synced on all machines.  One might then also want to perform load balancing.
But it seems that we could float this a little bit and allow for extending the kernel to a distributed kernel.

Again we could learn from Erlang.  Erlang has a different mode for distributed processes.  Each "core" gets a 
copy of the beam for running processes locally.  Really, we need only care about the model.  As long as we have a 
model of the machine performing in a truly concurrent manner, we needn't actually worry about the underlying 
implementation details.  Thus all that we would really need to do is write the code that coordinates multikernel setups.
And an implementation that used actual concurrency primitives, as long as it was a refinement of the semantics of the 
model machine (i.e. they are bisimilar) then we are fine.  We could then do things differently for concurrent, but on the 
same physical machine (fast, transactional memory sharing), versus concurrent, truly distributed machines.  What we would 
aim for is just that the model machine is a separation kernel, and that the refinement preserves separation.  We then 
should be getting confidentiality, integrity and dataflow preservation...

Thus we could have a secure, safe, concurrent, differentiable system.
-}
