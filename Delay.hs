module Delay where 

import Control.Monad.Free hiding (unfold)
-- However, the implementation of Generator using Free monads is slow, because the diagonalization takes a while.  The trick 
-- to asymptotically speed-up Free monads is to make it a codensity monad instead.  See 
-- https://www.janis-voigtlaender.eu/papers/AsymptoticImprovementOfComputationsOverFreeMonads.pdf
--  We'll use this speed-up at the end. 
-- Control.Monad.Codensity comes from the kan-extensions package (libghc-kan-extensions-dev on ubuntu).  However, note, 
-- this pulls in a lot of dependencies.  So feel free to comment this bit out, and also comment out the bits that use Codensity.
import Control.Monad.Codensity

import Control.Monad.State.Strict
import Control.Monad.Identity

-- data Free f a = Pure a | Free (f (Free f a))

{-
Hehe the Delay monad transformer is 
    data DelayT m a = Now a | Later (m (DelayT m a))
But that's exactly the Free monad on m.
But by realizing it's free, allows an asymptotic speedup.
-}
type DelayT m a = Free m a

{-
Interestingly, the algebras of the Delay monad, which is the free monad on the identity functor, are omega-cpos.
These are sufficient for encoding partial computation with while loops and countable recursion.  As Tarmo points out,
we don't need the extra power of going to a restriction category, which may be done, in intensional type theory.
-}
type Delay a = DelayT Identity a

{-
Typically, one should work in the codensity monad and not the Free monad, since
the former can be converted into the latter without loss.  In fact there is a type class
called FreeLike that lets you be formally agnostic.
-}
type FastDelayT m a = Codensity m a

type ResumptionT m a = DelayT m a
type FastResumptionT m a = FastDelayT m a 
type StatefulResumptionT s m a = ResumptionT (StateT s m) a
type FastStatefulResumptionT s m a = FastResumptionT (StateT s m) a
type StatefulResumption s a = StatefulResumptionT s Identity a 

type FastStatefulResumption s a = FastStatefulResumptionT s Identity a

step :: (Monad m) => m a -> ResumptionT m a 
step = lift 

stepFast :: (Monad m) => m a -> FastResumptionT m a 
stepFast = lift 