{ pkgs ? import <nixpkgs> {}}:
let 
  # get ghc with libraries bundled
  ghc = pkgs.haskellPackages.ghcWithPackages (p:[
    p.array
    p.base
    p.bound
    p.containers
    p.deriving-compat
    p.haskeline
    p.logict
    p.mtl
    p.text
    p.unification-fd
    p.alex
    p.happy 
    p.cabal-install
    p.free
    p.kan-extensions
    p.json
    p.aeson
  ]);
  alex = pkgs.haskellPackages.alex;
  happy = pkgs.haskellPackages.happy;
in 
  # pkg
  pkgs.mkShell {
    buildInputs = [ ghc pkgs.haskellPackages.alex pkgs.haskellPackages.happy pkgs.pkg-config pkgs.glib pkgs.haskellPackages.hpack];
    buildTools = [ pkgs.haskellPackages.alex pkgs.haskellPackages.happy pkgs.haskellPackages.hpack];
    buildToolDepends = [pkgs.haskellPackages.alex pkgs.haskellPackages.happy pkgs.haskellPackages.hpack];


    ALEX="${alex}/bin/alex";
    HAPPY="${happy}/bin/happy";
    shellHook=''
      export HAPPY=${happy}/bin/happy
      export ALEX=${alex}/bin/alex
      # The following is to force use of UTF-8 by default.
      export LC_ALL=C.UTF-8
    '';
    

  }
